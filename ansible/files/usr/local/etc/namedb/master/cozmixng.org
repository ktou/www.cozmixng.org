$TTL 86400	; 1 day
cozmixng.org.		IN SOA	ns1.cozmixng.org. postmaster.cozmixng.org. (
				2020102000 ; serial
				3600       ; refresh (1 hour)
				900        ; retry (15 minutes)
				604800     ; expire (1 week)
				86400      ; minimum (1 day)
				)
			NS	ns1.cozmixng.org.
			NS	ns2.cozmixng.org.
			A	160.29.167.10
			MX	10 mail.cozmixng.org.
			TXT	"v=spf1 mx ~all"
			SPF	"v=spf1 mx ~all"
;			AAAA	2001:2f8:c2:201::fff0
mail			A	49.212.136.173
;			AAAA	2001:2f8:c2:201::fff0
ml			A	160.29.167.10
			MX	10 mail
			TXT	"v=spf1 mx -all"
			SPF	"v=spf1 mx -all"
;			AAAA	2001:2f8:c2:201::fff0
ns1			A	160.29.167.10
			AAAA	2001:2f8:c2:201::fff0
ns2			A	160.29.167.14
			AAAA	2001:2f8:c2:201::fff4
pub			A	160.29.167.14
			MX	10 mail
			TXT	"v=spf1 a mx -all"
			SPF	"v=spf1 a mx -all"
;			AAAA	2001:2f8:c2:201::fff4
share			A	160.29.167.11
			MX	10 mail
;			AAAA	2001:2f8:c2:201::fff1
www			A	160.29.167.10
			MX	10 mail
;			AAAA	2001:2f8:c2:201::fff0
base			A	160.29.167.127

default._domainkey	IN	TXT	"v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC1HiML7/bpsgjfS/pMfSBqvw6iAqcyhQAxz/Ps1vvTjpHwYoct9qlL+JSSJOq5dPtfkQbbj0lXV0/Sf+3RfBPzoDkL6SBOPcIP1EF14YhWj6/0MfZmfu440sTFE13Z3XtMlImqOz6y7sPuM2r7cwrcyyjcgHlN206iQBn8wTQUtQIDAQAB"  ; ----- DKIM key default for cozmixng.org
_adsp._domainkey IN TXT "dkim=unknown"

*.ml			MX	10 mail.cozmixng.org.
			TXT	"v=spf1 mx -all"
			SPF	"v=spf1 mx -all"
